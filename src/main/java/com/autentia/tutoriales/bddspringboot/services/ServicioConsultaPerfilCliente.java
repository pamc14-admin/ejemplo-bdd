package com.autentia.tutoriales.bddspringboot.services;

import com.autentia.tutoriales.bddspringboot.vo.RespuestaPerfilCliente;

import java.util.UUID;

public interface ServicioConsultaPerfilCliente {
    RespuestaPerfilCliente consultar(UUID idCliente);
}
