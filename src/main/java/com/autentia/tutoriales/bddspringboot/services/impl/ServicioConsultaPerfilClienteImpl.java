package com.autentia.tutoriales.bddspringboot.services.impl;

import com.autentia.tutoriales.bddspringboot.services.ServicioConsultaPerfilCliente;
import com.autentia.tutoriales.bddspringboot.vo.RespuestaPerfilCliente;

import java.time.LocalDate;
import java.util.UUID;

public class ServicioConsultaPerfilClienteImpl implements ServicioConsultaPerfilCliente {

    @Override
    public RespuestaPerfilCliente consultar(UUID idCliente) {

        return RespuestaPerfilCliente.builder()
                .idCliente(idCliente)
                .nombre("David")
                .fechaNacimiento(LocalDate.of(1976, 2, 28))
                .email("dgarciagil@autentia.com")
                .telefono("+34 123456789")
                .build();
    }
}
