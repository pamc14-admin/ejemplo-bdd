package com.autentia.tutoriales.bddspringboot.vo;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Builder
public class RespuestaPerfilCliente {
    private UUID idCliente;
    private String nombre;
    private LocalDate fechaNacimiento;
    private String email;
    private String telefono;
}
